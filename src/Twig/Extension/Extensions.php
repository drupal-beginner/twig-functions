<?php

namespace Drupal\twig_functions\Twig\Extension;

/**
 * Custom twig extensions.
 */
class Extensions extends \Twig_Extension
{

  /**
   * {@inheritdoc}
   * This function must return the name of the extension. It must be unique.
   */
  public function getName()
  {
    return 'twig_functions.twig_extension';
  }


  /**
   * {@inheritdoc}
   */
  public function getFunctions()
  {
    $functions = [];

    return $functions;
  }
}
